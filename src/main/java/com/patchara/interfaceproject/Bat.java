/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.interfaceproject;

/**
 *
 * @author user1
 */
public class Bat extends Poultry {

    public Bat() {
        super();
    }

    @Override
    public void eat() {
        System.out.println("Bat: Eat");
    }

    @Override
    public void speak() {
        System.out.println("Bat: Speak");
    }

    @Override
    public void sleep() {
        System.out.println("Bat: Sleep");
    }

    @Override
    public void fly() {
        System.out.println("Bat: Fly");
    }

}

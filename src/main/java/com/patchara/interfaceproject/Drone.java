/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.interfaceproject;

/**
 *
 * @author user1
 */
public class Drone extends Vahicle implements Flyable,Runable{

    public Drone() {
        super();
    }

    @Override
    public void startEngine() {
        System.out.println("Drone: Start engine");

    }

    @Override
    public void stopEngine() {
        System.out.println("Drone: Stop engine!");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Drone: Rise speed!");
    }

    @Override
    public void applyBreak() {
        System.out.println("Drone: Apply Break!");
    }

    @Override
    public void fly() {
        System.out.println("Drone: Fly!");

    }

    @Override
    public void run() {
        System.out.println("Drone: Run!");
    }

}

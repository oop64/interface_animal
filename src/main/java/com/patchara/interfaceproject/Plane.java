/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.interfaceproject;

/**
 *
 * @author user1
 */
public class Plane extends Vahicle implements Flyable,Runable{

    public Plane() {
        super();
    }

    @Override
    public void startEngine() {
        System.out.println("Plane: Start engine");

    }

    @Override
    public void stopEngine() {
        System.out.println("Plane: Stop engine!");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Plane: Rise speed!");
    }

    @Override
    public void applyBreak() {
        System.out.println("Plane: Apply Break!");
    }

    @Override
    public void fly() {
        System.out.println("Plane: Fly!");

    }

    @Override
    public void run() {
        System.out.println("Plane: Run!");
    }

}

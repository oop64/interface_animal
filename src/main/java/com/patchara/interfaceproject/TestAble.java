/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.interfaceproject;

/**
 *
 * @author user1
 */
public class TestAble {

    public static void main(String[] args) {
        Bat bat = new Bat();
        Plane plane = new Plane();
        Dog dog = new Dog();
        Bird bird = new Bird();
        Drone drone = new Drone();
        Cat cat = new Cat();
        
        Flyable[] flyable  = {bat, bird, plane, drone};
        for(Flyable f : flyable){
            if(f instanceof Plane){
                Plane p = (Plane)f;
                p.startEngine();
                p.run();
            }
            else if(f instanceof Drone){
                Drone d = (Drone)f;
                d.startEngine();
            }
            f.fly();
            newLine();
        }
        
        Runable[] runable  = {dog, cat, plane};
        for(Runable r : runable){
            if(r instanceof Plane){
                Plane p = (Plane)r;
                p.startEngine();
            }   
            else if(r instanceof Drone){
                Drone d = (Drone)r;
                d.startEngine();
            }
            r.run();
            newLine();
        }
        
    }

    private static void newLine() {
        System.out.println();
    }
}
